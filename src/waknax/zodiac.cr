require "struct-gen"

require "./with"

StructGen.generate_struct Waknax::Zodiac, {
  is_module:       false,
  is_required:     true,
  attribute_name:  id,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  serialized_name: "begin",
  attribute_name:  begin_date,
  attribute_class: Time,
}, {
  is_module:       false,
  is_required:     true,
  serialized_name: "end",
  attribute_name:  end_date,
  attribute_class: Time,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Waknax,
  attribute_name:  color,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  background,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  serialized_name: "background_url",
  attribute_name:  background_url,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  name,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  description,
  attribute_class: String,
}, {
  is_module:       true,
  is_required:     true,
  attribute_name:  image_url,
  base_module:     Waknax,
  attribute_class: String,
}
