require "struct-gen"

require "./event"
require "./zodiac"
require "./month"

StructGen.generate_struct Waknax::Entry, {
  is_module:       false,
  is_required:     true,
  attribute_name:  event,
  attribute_class: Waknax::Event,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  zodiac,
  attribute_class: Waknax::Zodiac,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  month,
  attribute_class: Waknax::Month,
}
