require "struct-gen"

StructGen.generate_with_required_field Waknax, Id, id, UInt16, "id"
StructGen.generate_with_required_field Waknax, ImageUrl, image_url, String, "image_url"
StructGen.generate_with_required_field Waknax, Color, color, String, "color"
