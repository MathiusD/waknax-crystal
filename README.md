# Waknax - Crystal

[![Pipeline Status](https://gitlab.com/MathiusD/waknax-crystal/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/waknax-crystal/-/pipelines)
[![Documentation](https://img.shields.io/badge/docs-available-brightgreen.svg)](https://mathiusd.gitlab.io/waknax-crystal)

Waknax with definitions for public data of Almanax of MMORPG Wakfu
