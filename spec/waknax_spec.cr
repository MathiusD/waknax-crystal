require "./spec_helper"
require "http"

describe Waknax do
  it "fetch-current" do
    json_url = "https://haapi.ankama.com/json/Ankama/v2/Almanax/GetEvent?lang=fr"
    response = HTTP::Client.get json_url
    if response.status_code != 200
      raise "Current Almanax Data (#{json_url}) unreachable ! (Status code received : #{response.status_code})"
    end
    entry = Waknax::Entry.from_json(response.body)
    entry.should be_truthy
  end
end
