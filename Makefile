clean:
	@rm -rf docs
	@rm -rf reports
	@rm -rf lib
	@rm -f shard.lock

install:
	@shards install

tests: 
	@crystal spec --junit_output "reports/regular_tests-$(shell date '+%s').xml"
	
format:
	@crystal tool format --check

docs:
	@crystal docs src/waknax.cr